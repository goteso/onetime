<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportedUsers extends Model
{
        protected $fillable = [ 'reported_user_id', 'user_id', 'status'];
		protected $table = 'reported_users';
		
		
			    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
}