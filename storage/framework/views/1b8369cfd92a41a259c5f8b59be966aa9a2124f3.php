<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>User Orders</strong>
          </h1>
    
        </section>
        
<div class="container-fluid order-detail">
 
 		 <div class="row  user-profile-data"  > 
	      <div class="col-sm-12 col-md-6 col-lg-4"   >
		    <div class="card info"   >
		     <img src="<?php echo url('/')."/";?>users/<?php echo $user[0]->profile_image;?>" style="display:block;margin-right:0;max-height:200px">
			 <div>
			       <h4> <?php echo e(@$user[0]->first_name); ?> <?php echo e(@$user[0]->last_name); ?> <a href="<?php echo e(URL::to('admin/user/'.$user[0]->id.'/edit')); ?>"><i class="fa fa-edit"></i></a></h4>
			   <h5><?php echo e(@$user[0]->username); ?></h5>
			 </div>
		   </div>
		 </div> 
 	 
		   <div class="col-sm-6 col-md-3"   >
		    <div class="card"   >
			<div class="card-data">
		       <h4><?php echo e(@$user[0]["total_orders"]); ?> Orders</h4>
			   <h5>In Progress</h5>
			 </div>
		   </div>
		   </div>
		  <div class="col-sm-6 col-md-3"   >
		   <div class="card"    >
		   <div class="card-data">
		      <h4> <?php echo e(@$user[0]['total_trades']); ?> Trades</h4>
			 <h5>In Progress </h5>
			 </div>
		   </div>
		   </div> 
		 
		 
		 </div>
		 <br><br>
		 
  <div class="row">
    <div class="col-sm-12">

	
	<?php echo $__env->make('admin.users.users_tabs_bar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>    
	
	 
 <!-------------------orders table starts here----->
  <table id="example1" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
                        <th>OrderId</th>
						<th>Seller</th>
                        <th>Price</th>
                        <th>Product</th>
						<th>Status</th>
					    <th>Created</th>
                        <th id="action" style="display:none ">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($data as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
					 <td>#<?php echo e(@$value['id']); ?></td>
                    <td><?php echo e(@$value['seller_name']); ?></br> <?php echo e(@$value['seller_email']); ?></br><?php echo e(@$value['seller_phone']); ?></td>
                  	<td> <?php echo e(@$value['post_currency']); ?> <?php echo e(@$value['order_price']); ?> 	</td>
					<td> <?php echo e(@$value['post_title']); ?>  	</td>
					<td> <?php echo e(@$value['status']); ?>  	</td>
			        <td><?php echo $value['created_at']; ?></td>
					
					
					
					
					
				
                    <td class="action-btn" style="display:none">
                     <a class="btn btn-small  " title="View User" href="<?php echo e(URL::to('admin/user/show/'.$value['id'])); ?>"  target="_blank" ><i class="fa fa-eye"></i></a>           
                   <a class="btn btn-small  " title="Edit User" href="<?php echo e(URL::to('admin/user/'.$value['id'].'/edit')); ?>" target="_blank"><i class="fa fa-edit"></i></a>                  
                  <a class="btn btn-small  " title="Delete User" href="<?php echo e(URL::to('admin/user/'.$value['id'].'/delete')); ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>   

				  <?php if($value['status'] == '1') { ?>
				  
                   <a class="btn btn-small  " title="Block User" href="<?php echo e(URL::to('admin/user/'.$value['id'].'/block')); ?>" onclick="return confirm('Are you sure to Block this User?')"><i class="fa fa-ban"></i></a>  
				  <?php } else { ?>
                   <a class="btn btn-small  " title="Unblock User" href="<?php echo e(URL::to('admin/user/'.$value['id'].'/unblock')); ?>" onclick="return confirm('Are you sure to Unblock this User?')"><i class="fa fa-unlock"></i></a>  
				  <?php } ?>
				  
				  
				 
 
            	   </td>
                   	 </tr>
					 
					 
					 
					 
 



                   <?php }?>
                   
                   </tbody>
 </table>
   <?php echo e($data->links()); ?>

 
  <!-------------------orders table ends here----->
    </div>  
  </div>
  
  
</div>

      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>