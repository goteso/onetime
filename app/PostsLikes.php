<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostsLikes extends Model
{
        protected $fillable = [ 'user_id','post_id'];
		protected $table = 'posts_likes';
		
		
	public function posts()
    {
        return $this->belongsTo('App\Posts','id');
    }
	
	
	
	
	
}