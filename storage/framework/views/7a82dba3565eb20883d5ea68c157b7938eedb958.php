<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper"  >
        <!-- Content Header (Page header) -->
 
 
<div class="container" style="margin:0;padding:0">
 <div class="row">
    <div class="col-sm-12"  >
	    <h2>Buying Trade Detail #<?php echo e($data[0]['id']); ?></h2>
	</div>
	</div>
  <div class="row"  >
    <div class="col-sm-4">
	<div class="panel delivery-detail">
	   <h3>Order Recipt</h3>
       <table class="table">
	   <tbody>
	   <tr>
		   <td><h4><span class="currency">EUR</span><?php echo e($data[0]['trade_rate']); ?> @ <span class="currency">EUR</span>44/BTC</h4></td>
		   <td> </td> 
		 </tr>
	     <tr> 
		   <td>Trade Created</td>
		   <td><?php echo e($data[0]['id']); ?></td>
		 </tr>
		  <tr> 
		   <td>Payment Transfered</td>
		   <td >2018-05-28 08:43:05</td>
		 </tr> 
 
		 </tbody>
	   </table>
	</div> 
    </div> 
	
	
	
	
	<div class="col-sm-4">
      <div class="panel">
	   <h3>Seller Details</h3>
      <p class="title">Sold By</p>
      <p> <?php echo e($data[0]['seller_name']); ?></p>
	  <hr style="margin:10px 0;">
	    <p class="title">Bank Details</p>
      <p><?php echo e($data[0]['bank_details']); ?></p>
	</div> 
    </div>
	
	
	 <div class="col-sm-4">
      <div class="panel">
	   <h3>Buyer Details</h3>
      <p class="title">Created and Bought By</p>
      <p> <?php echo e($data[0]['final_buyer_name']); ?>  India</p>
	</div> 
    </div> 
	
	 </div>
  </div>
</div><?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>