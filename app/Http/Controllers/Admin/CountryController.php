<?php

namespace App\Http\Controllers\Admin; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\Country;
use App\User;
use Mail;
use Hash;
 
 
 


class CountryController extends Controller 
{
    
   
   public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(request $request)
    {
        // get all the countries
        $countries = Country::orderBy('id','desc')->paginate(10);  
		if ($request->ajax()) {
            return view('admin.countries.data-ajax', compact('countries'));
        }
 		return view('admin.countries.index')->with('countries', $countries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
      $countries = DB::table('countries')->get();        
      return view('admin.countries.create')->with('countries',$countries);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'currency' => 'required|max:255|alpha',
        'country_name' => 'required|max:255|alpha',
        'nicename' => 'required',
         ]);
        if ($validator->fails()) {
          //print_r($validator->errors()->all());die;
            return redirect('/admin/country/create')
                ->withInput()
                ->withErrors($validator);
        }

        $country = new Country;
        $country->country_name  = $request->country_name;
        $country->nicename   = $request->nicename;
		$country->currency   = $request->currency;
        $country->save();
        // redirect
        Session::flash('message', 'Successfully created country!');
        return redirect('admin/country');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {   
        if(!is_numeric($id))
        {
         return redirect('admin/country');   
        }
        // get the testimonial
        $country = Country::find($id);

        // show the view and pass the nerd to it
        return view('admin.countries.show')
            ->with('country', $country);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if(!is_numeric($id))
        {
         return redirect('admin/country');   
        }
        $countries = DB::table('countries')->get();   
        $country = Country::find($id);

        // show the edit form and pass the nerd
        return view('admin.countries.edit')
            ->with('country', $country)->with('countries', $countries);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id , Request $request)
    {
                // validate
        if(!is_numeric($id))
        {
         return redirect('admin/country');   
        }
        $validator = Validator::make($request->all(), [
        'first_name' => 'required|max:255|alpha',
        'last_name' => 'required|max:255|alpha',        
        'country_id' => 'required',
         ]);
        if ($validator->fails()) {
          //print_r($validator->errors()->all());die;
            return redirect('/admin/country/'.$id.'/edit')
                ->withInput()
                ->withErrors($validator);
        }
         else {
            // store
            $country = Country::find($id);
            $country->first_name  = $request->first_name;
            $country->last_name   = $request->last_name;
            $country->email  = $request->email;
            $country->country_id   = $request->country_id;                                    
            $country->save();

            // redirect
            Session::flash('message', 'Successfully updated country!');
            return redirect('admin/country');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        
        $country = Country::find($id);
        $country->delete();
        // redirect
        Session::flash('message', 'Successfully deleted the country!');
        return redirect('admin/country');
    }
	
	public function block($id ,  $requesting_user_id)
	{
	  
	 
	        $this->create_notification('block','country is blocked',$requesting_user_id,$id,$is_anonymous = 0,$que_id = 0,$answer_id = 0);
	        $country = Country::find($id);
            $country->status  = '0';
            $country->save();
			Session::flash('message', 'Successfully Blocked the country!');
			return back();
	}
	
	
	
	
	
	
	
 
	
	
	
	
	
	
	

}