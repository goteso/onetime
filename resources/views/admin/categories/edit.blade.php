@extends('layout.admin')
@section('content')
  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       <section class="content-header">
          <h1>
            <strong>Users</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/user') }}">Users</a></li>
            <li class="active">Edit</li>
          </ol>
        </section

        <!-- Main content -->
        <section class="content">
          <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admin/user') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i> View All </a>
                </div><!-- /.box-header -->
            </div>
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">

              <!-- general form elements -->
              <div class="box box-primary">

                <div class="box-header with-border">
                  <h3 class="box-title">Edit User</h3>
                </div><!-- /.box-header -->
                <!-- form start -->                
                <?php echo Form::open(array('url' => 'admin/user/'.$user->id,'method'=>'put','class'=>'form-horizontal')) ?>

                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="first_name" class="col-md-3 control-label">First Name</label>
                     <div class="col-md-6">  
                      <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter First Name" value="<?php echo $user->first_name;?>">
                      <div class="error-message">{{ $errors->first('first_name') }}</div>
                    </div>
                    </div>

                    <div class="form-group">
                      <label for="last_name" class="col-md-3 control-label">Last Name</label>
                      <div class="col-md-6"> 
                      <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter Last Name" value="<?php echo $user->last_name;?>">
                      <div class="error-message">{{ $errors->first('last_name') }}</div>
                    </div>
                    </div>


                    <div class="form-group">
                      <label for="email" class="col-md-3 control-label">Email</label>
                      <div class="col-md-6"> 
                      <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email" value="<?php echo $user->email;?>" readonly>
                      <div class="error-message">{{ $errors->first('email') }}</div>
                    </div>
                    </div>

                    <div class="form-group">
                      <label for="country_id" class="col-md-3 control-label">Select Country</label>
                      <div class="col-md-6"> 
                      <select name="country_id" id="country_id" class="form-control" class="col-md-3 control-label">
                        <option >Select Country</option>
                        <?php foreach ($countries as $key => $value) {
                        ?>
                        <option value="<?php echo $value->id;?>" <?php if($value->id==$user->country_id){ ?> selected="selected"<?php }?>><?php echo $value->country_name;?></option>
                        <?php
                        }?>
                      </select>
                      <div class="error-message">{{ $errors->first('country_id') }}</div>
                    </div>
                    </div>

                    <!-- <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password">
                      <div class="error-message">{{ $errors->first('password') }}</div>
                    </div>


                    <div class="form-group">
                      <label for="password_confirmation">Confirm Password</label>
                      <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Enter Confirm Password">
                      <div class="error-message">{{ $errors->first('password_confirmation') }}</div>
                    </div>
                    
                  </div>-->

                  <!-- /.box-body -->

                    <div class="box-footer">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop