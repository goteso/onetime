<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use App\Auction;
use App\AuctionBids;
use Carbon;
 
use App\ScheduledJobs;
 
 
use App\Traits\one_signal; // <-- you'll need this line...
 
 
 


class ScheduledJobsController extends Controller 
{
	
 

use one_signal;  
 
   public function get_bidders_token_array($auction_id,$user_id)
    {  
	   //$token_array = AuctionBids::distinct()->where('auction_id',$auction_id)->where('user_id','<>',$user_id)->get(["user_id"])->toArray();
$token_array = AuctionBids::distinct()->where('auction_id',$auction_id)->get(["user_id"])->toArray();
	   $token_array_final = array();
          for($i=0;$i<sizeof($token_array);$i++)
              {
				   $token = User::where('id',$token_array[$i]["user_id"])->first(["notification_token"])->notification_token;
				   $is_notification_on = User::where('id',$token_array[$i]["user_id"])->first(["is_notification_on"])->is_notification_on;
				   if($token != '' && $token != null && $token != NULL && $is_notification_on == '1' )
				   {
	                              $token_array_final[] = $token;
				   }
              }
           $token_array_final[] = User::where('id',$user_id)->first(["notification_token"])->notification_token;
	   return $token_array_final;
	   return false;
    }
	

  //this function is used to add scheduled job into the database
    public function add_job(Request $request)
    {
		    $input = $request->all();
                    $auction = ScheduledJobs::create($input);
                    return 'inserted';
    }




  //this function is used to add scheduled job into the database
    public function edit_job(Request $request)
    {
		
		 
    }




  //this function is used to add scheduled job into the database
    public function delete_job(Request $request)
    {
		
		 
    }





  //this function is used to add scheduled job into the database
    public function check_auction_expiry()
    {

     $current_date_time = Carbon\Carbon::now();
     $date_time= Carbon\Carbon::now();

     $date =$date_time;
     $date->modify('-1 minutes');
     $formatted_date = $date->format('Y-m-d H:i:s');
 
     $result = DB::table('scheduled_jobs')->where('calling_time','>',$formatted_date)->where('calling_time','<',$current_date_time )->where('type','=','check_auction_expiry')->get()->toArray();
     //$result = DB::table('scheduled_jobs')->where('calling_time','<',$current_date_time )->where('type','=','check_auction_expiry')->get();
   

         foreach ($result as $job) {
                                    $auction_id = $job->request_data;
                                    $auction = Auction::where('id', $auction_id )->get();
	                            $auctioner_id = $auction[0]["user_id"];
                                    $auction_title = $auction[0]["title"];

 
                                    $token_array = $this->get_bidders_token_array($auction_id , $auctioner_id);

                                    $res = $this->notification_to_array($token_array, 'Auction Closed: '.$auction_title.'-'.$auction_id , 'This Auction is Closed Now ');
                                    $auction = Auction::where('id', $auction_id )->update(['status'=>2]);
 
                                   }
    }





}
