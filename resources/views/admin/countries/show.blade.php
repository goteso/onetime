@extends('layout.admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Country Details</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/countries') }}">Countries</a></li>
            <li class="active">View</li>
          </ol>
        </section
        
        <section class="content">
           <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admin/country') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i>View All</a>
                </div><!-- /.box-header -->
            </div>

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                 <table  class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th width="15%">Country Name(Google Map)</th>
                         <td><?php echo $country->country_name;?></td>
                          </tr>
                          <tr>
                        <th width="15%">Display Name</th>
                         <td><?php echo $country->nicename;?></td>
                          </tr>
                           <tr>
                        <th width="15%">Currency Symbol</th>
                        <td><?php echo $country->currency;?></td>
                         </tr>

 
                    </thead>
                    <tbody>
                                                      
                    </tbody>
                    
                  </table>
             
            </div><!-- /.box-body -->
           
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop