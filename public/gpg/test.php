<?php

require __DIR__ . '/vendor/autoload.php';
require_once 'libs/GPG.php';

$gpg = new GPG();
 
$public_key_ascii = '-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: BCPG C# v1.6.1.0

mQENBFrayWwBCAC+G7mjD0iqoO8ahoEI7Cxl+E5zOraWPDirU8bpEybmvixajalk
BAbbbCbzDZ0tgQZJ6mZM//i4edd33e6vJlx7DGMyrXm5HpMzWd3fDtVaK1ENP07K
6kPlVzFLoCiFwKNHlxJAJzDd1BHy1ckUEhuDy71pJjWHT0h/FOTjITjW8nt0q7/o
ccSE51bPWwoiMwto9VqrT5ZGZdedy4S4/+rGuNYJ1QMlW5zaw2zF2DnLXF44tQQE
y+F58R94mlDC5t793iBj9kw0I7AcGgIn2lEGDtkp7VPKKsqfcAmgbAvsqQa6AATo
8arW0okXDgJjI7proVsBl0AGNfRgdDRlhKtZABEBAAG0HWhhcnZpbmRlcnNpbmdo
MjUwMDBAZ21haWwuY29tiQEcBBABAgAGBQJa2slsAAoJEAWauDYve9kK71UIAJDg
A6WEvpDHEqUZqgtFItpWajBXcG4dzueCfW3FU7E82Cii5bxjzw/aW30pogAAJ+mB
CB/4O64v+Ma0bhde1slOf0aILXFOFy0IAizB4/9sdUiisSNTBOSEWT1lwcFIetq+
eA2+3itcjKlwpiPjuxI/yoTNOwo06uCguhtR8D0uP3Z/lcsEn+7+fbnpqa4vice6
HL8tCzyXSqTahHtx7Vsu/Vh5yWoW2aZaINlvuYc0V+mwJ094TmHWlufvDmwDId7I
9EcBcj3AE6a30QwpwQNJ54Kd4xsG2G3x+MNpGBEohaTGegwfJhv3MtPm7QAKePzc
DzFym2wFZTDHToi4b+s=
=6GFL
-----END PGP PUBLIC KEY BLOCK-----';

 
// create an instance of a GPG public key object based on ASCII key
$pub_key = new GPG_Public_Key($public_key_ascii);

$plain_text_string = 'Deepu Rani';
// using the key, encrypt your plain text using the public key
$encrypted = $gpg->encrypt($pub_key,$plain_text_string);

echo $encrypted;

 



 

?>