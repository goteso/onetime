<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRatings extends Model
{
        protected $fillable = [
        'user_id','profile_user_id','ratings'
    ];
    
	
		    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
 
    
}
