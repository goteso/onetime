<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostsImages extends Model
{
        protected $fillable = [ 'post_id', 'image'];
		protected $table = 'posts_images';
		
		
	public function posts()
    {
        return $this->belongsTo('App\Posts','id');
    }
	
	
	
	
}