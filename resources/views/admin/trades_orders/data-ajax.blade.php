                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
					    <th>Title</th>
                        <th>User</th>
                        <th>Seller</th>
                        <th>Created</th>
                        <th id="action">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($result as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
					<td><?php echo $value['seller_name']; ?></td>
                    <td style="word-wrap: break-word;width:50%"><?php echo $value['seller_name']; ?></td>
                    <td> 
                   	 
                   	</td>
				 
                    <td><?php echo $value['created_at']; ?></td>
                    <td>
                               
                   <a class="btn btn-small btn-info" title="Edit User" href="{{ URL::to('/posts_list/'.$value['id'].'/edit') }}"><i class="fa fa-pencil"></i></a>                  
                  <a class="btn btn-small btn-danger" title="Delete User" href="{{ URL::to('/posts_list/'.$value['id'].'/delete') }}" onclick="return confirm('Are you sure?')"><i class="fa fa-remove"></i></a>   
 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                