<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportedPosts extends Model
{
        protected $fillable = [ 'user_id', 'post_id', 'status'];
		protected $table = 'reported_posts';
		
			    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
}