<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaypalCurrencyCodes extends Model
{
        protected $fillable = [ 'currency','currency_code','currency_symbol'];
		protected $table = 'paypal_currency_codes';
}