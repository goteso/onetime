<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2017-2018 <a href="#">{{env('APP_NAME')}}</a>.</strong> All rights reserved.
		
		<div class="visible-print text-center">
    {{ QrCode::size(100)->generate(Request::url()) }}
    <p>Scan me to return to the original page.</p>
</div>


      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
       
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
    </div><!-- ./wrapper -->
    <script type="text/javascript">
        var editor = CKEDITOR.replace( 'editor1', {
          removePlugins: 'image',
        });
   
      </script>
      <script>
       $(document).ready(function() {
      $('#example1').DataTable({"bPaginate": false});      
      $("#action").removeAttr('class');
      $("#action").removeAttr('style');
      $("#action").removeAttr('aria-controls');
      $("#action").removeAttr('aria-label');
      $("#action").removeAttr('aria-sort');

      });
      </script>

     
    