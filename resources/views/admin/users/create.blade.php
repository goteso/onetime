@extends('layout.admin')
@section('content')
  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       <section class="content-header">
          <h1>
            <strong>Users</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/user') }}">Users</a></li>
            <li class="active">Add</li>
          </ol>
        </section

        <!-- Main content -->
        <section class="content">
          <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admin/user') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i> View All </a>
                </div><!-- /.box-header -->
            </div>
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">

              <!-- general form elements -->
              <div class="box box-primary">

                <div class="box-header with-border">
                  <h3 class="box-title">Add user</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {{ Form::open(array('url' => 'admin/user','class'=>'form-horizontal')) }}
                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="first_name" class="col-md-3 control-label">First Name</label>
                       <div class="col-md-6">  
                      <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter First Name" value="{{old('first_name')}}">
                      <div class="error-message">{{ $errors->first('first_name') }}</div>
                    </div></div>

                    <div class="form-group">
                      <label for="last_name" class="col-md-3 control-label">Last Name</label>
                      <div class="col-md-6">  
                      <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter Last Name" value="{{old('last_name')}}">
                      <div class="error-message">{{ $errors->first('last_name') }}</div>
                    </div>
                    </div>


                    <div class="form-group">
                      <label for="email" class="col-md-3 control-label">Email</label>
                      <div class="col-md-6">  
                      <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email" value="{{old('email')}}">
                      <div class="error-message">{{ $errors->first('email') }}</div>
                    </div>
                  </div>

                    <div class="form-group">
                      <label for="country_id" class="col-md-3 control-label">Select Country</label>
                      <div class="col-md-6">  
                      <select name="country_id" id="country_id" class="form-control" class="col-md-3 control-label" value="{{old('country_id')}}">
                        <option value="">Select Country</option>
                        <?php foreach ($countries as $key => $value) {
                        ?>
                        <option <?php if($value->id == old('country_id') ){?> selected="selected" <?php } ?> value="<?php echo $value->id;?>"><?php echo $value->country_name;?></option>
                        <?php
                        }?>
                      </select>
                      <div class="error-message">{{ $errors->first('country_id') }}</div>
                    </div>
                  </div>

                     <div class="form-group">
                      <label for="password" class="col-md-3 control-label">Password</label>
                      <div class="col-md-6">  
                      <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" maxlength="20">
                      <div class="error-message">{{ $errors->first('password') }}</div>
                    </div>
                  </div>

                    <div class="form-group">
                      <label for="password_confirmation" class="col-md-3 control-label">Confirm Password</label>
                      <div class="col-md-6">  
                      <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" maxlength="20" placeholder="Enter Confirm Password">
                      <div class="error-message">{{ $errors->first('password_confirmation') }}</div>
                    </div>
                    </div>
					
					
					
					       <div class="form-group">
                      <label for="phone" class="col-md-3 control-label">Phone</label>
                      <div class="col-md-6">  
                      <input type="text" name="phone" class="form-control" id="phone" maxlength="20" placeholder="Users Phone">
                      <div class="error-message">{{ $errors->first('phone') }}</div>
                    </div>
                    </div>
					
					
					       <div class="form-group">
                      <label for="address" class="col-md-3 control-label">Address</label>
                      <div class="col-md-6">  
                        <textarea rows="5" name="address" class="form-control" id="address"   placeholder="Users Address"></textarea>
                      <div class="error-message">{{ $errors->first('password_confirmation') }}</div>
                    </div>
                    </div>
					
					
					       <div class="form-group">
                      <label for="about" class="col-md-3 control-label">About</label>
                      <div class="col-md-6">  
                      <textarea rows="5" name="about" class="form-control" id="about"   placeholder="Users Bio"></textarea>
                      <div class="error-message">{{ $errors->first('about') }}</div>
                    </div>
                    </div>
					
					
					       
                  </div><!-- /.box-body -->

                   <div class="box-footer">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop