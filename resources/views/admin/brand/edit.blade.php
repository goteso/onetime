@extends('layout.admin')
@section('content')
  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       <section class="content-header">
          <h1>
            <strong>Edit Brand</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('brands/brand') }}">Brands</a></li>
            <li class="active">Edit</li>
          </ol>
        </section

        <!-- Main content -->
        <section class="content">
          <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('brands/brand') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i> View All </a>
                </div><!-- /.box-header -->
            </div>
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">

              <!-- general form elements -->
              <div class="box box-primary">

                <div class="box-header with-border">
                  <h3 class="box-title">Edit Brand</h3>
                </div><!-- /.box-header -->
                <!-- form start -->                
                <?php echo Form::open(array('url' => 'brands/brand/'.$user->id,'method'=>'put','class'=>'form-horizontal','files'=>'true')) ?>

                
                  <div class="box-body">
				  
				  			<?php if($user->photo == '')
			{
				echo '<h4 class="text-center" style="height:160px;width:150px;border:1px solid #ccc;border-radius:5px;padding-top:50px;">Upload Profile pic</h4>';
			}
			else
			{
?>

       <div class="form-group">
                      <label for="first_name" class="col-md-3 control-label"></label>
                     <div class="col-md-6">  
                     	<img src="<?php echo url('/').'/'.$user->photo;?>" height="200px" >
                    </div>
                    </div>
					
					
		
			<?php }
			?>
			
			
                    <div class="form-group">
                      <label for="first_name" class="col-md-3 control-label">Title</label>
                     <div class="col-md-6">  
                      <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter First Name" value="<?php echo $user->first_name;?>">
                      <div class="error-message">{{ $errors->first('first_name') }}</div>
                    </div>
                    </div>
 


                    <div class="form-group">
                      <label for="email" class="col-md-3 control-label">Email</label>
                      <div class="col-md-6"> 
                      <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email" value="<?php echo $user->email;?>" readonly>
                      <div class="error-message">{{ $errors->first('email') }}</div>
                    </div>
                    </div>

         
							  
				      <div class="form-group">
                      <label for="email" class="col-md-3 control-label">Image</label>
                      <div class="col-md-6">  
                      <?php   echo Form::file('image');?>
                    </div>
                  </div>
					
					         <div class="form-group">
                      <label for="status" class="col-md-3 control-label">Select Status</label>
                      <div class="col-md-6">  
                      <select name="status" id="status" class="form-control" class="col-md-3 control-label" value="{{ $user->status }}">
                        <option value="">Select Status</option>
                        
                        <option <?php if('1' == $user->status ){?> selected="selected" <?php } ?> value="1">Active</option>
						<option <?php if('0' == $user->status ){?> selected="selected" <?php } ?> value="0">Disabled</option>
                        
                      </select>
                      <div class="error-message">{{ $errors->first('status') }}</div>
                    </div>
                  </div>
				  
				  

                    <!-- <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password">
                      <div class="error-message">{{ $errors->first('password') }}</div>
                    </div>


                    <div class="form-group">
                      <label for="password_confirmation">Confirm Password</label>
                      <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Enter Confirm Password">
                      <div class="error-message">{{ $errors->first('password_confirmation') }}</div>
                    </div>
                    
                  </div>-->

                  <!-- /.box-body -->

                    <div class="box-footer">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop