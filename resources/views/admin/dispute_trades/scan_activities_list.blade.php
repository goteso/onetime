@extends('layout.admin')
@section('content')
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         <strong>Scan Activities</strong>
      </h1>
 
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-info">
         <div class="box-header" style="display:none">
            <nav class="navbar navbar-default" style="display:none">
               <!---<div class="container-fluid">
                  <div class="navbar-header">
                    <b class="navbar-brand" >Filter By Status :</b>
                  </div>
                  <ul class="nav navbar-nav">
                  <li><a href="{{ URL::to('admins/admin/status/1') }}">Active({{ \App\Admin::where(['status' => 1])->get()->count() }})</a></li>
                    <li><a href="{{ URL::to('admins/admin/status/0') }}">Blocked({{ \App\Admin::where(['status' => 0])->get()->count() }})</a></li>
                  </ul>
                  
                  </div>-->
            </nav>
 
         </div>
         <!-- /.box-header -->
      </div>
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">Scan Activities</h3>
               </div>
               <!-- /.box-header -->
               @if(Session::has('message'))
               <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif
               <div id="loading"></div>
               <style>
                  #loading {
                  position: fixed;
                  top: 50%;
                  left: 50%;
                  -webkit-transform: translate(-50%, -50%);
                  transform: translate(-50%, -50%);
                  }
               </style>
               <div id="item-lists">
                  <div class="box-body">
                     <table id="example1" class="table table-bordered table-striped">
                        <thead>
                           <tr>
                              <th>Sr. No.</th>
                              <th>User</th>
                              <th>Store</th>
                              <th>Offer</th>
                              <th>Approved ?</th>
                              <th>Consumed ?</th>
                              <th>Created</th>
                    
                           </tr>
                        </thead>
                        <tbody>
                           <?php   $sr = 0; foreach ($posts as $key => $value) { ?>
                           <tr>
                              <td><?php echo ++$sr; ?></td>
                              <td><?php 
							       $user_first_name = \App\User::where('id',$value['user_id'])->first(['first_name'])->first_name;
							       $user_last_name = \App\User::where('id',$value['user_id'])->first(['last_name'])->last_name;
								   echo $user_first_name.' '.$user_last_name;
							  
							  ?></td>
                              <td><?php   $store_long_address = @\App\Admin::where('id',$value['store_id'])->first(['long_address'])->long_address; echo @$store_long_address; ?>
                              </td>
                              <td style=""><?php $offer_title = @\App\Posts::where('id',$value['post_id'])->first(['title'])->title; echo @$offer_title; ?></td>
                              <td><?php
                                 if($value['approved_status'] == 0)
                                 	{
                                 	echo "Not Approved";
									?></br>
									<a class="btn btn-small btn-success" title="Delete User" href="{{ URL::to('/scan_activities_approve/'.$value['id'].'/scan_activities_approve') }}" onclick="return confirm('Are you sure?')">Approve</a> 
                                  <?php }
                                   else
                                   {
                                    echo "Approved";
                                 } ?>
                              </td>
							  
							               <td><?php
                                 if($value['consumed_status'] == 0)
                                 	{
                                 	echo "Not Consumed";
                                   }
                                   else
                                   {
                                    echo "Consumed";
                                 } ?>
                              </td>
							  
							  
                 
                              <td><?php echo $value['created_at']; ?></td>
                     
                           </tr>
                           <?php }?>
                        </tbody>
                     </table>
                  </div>
                  <!-- /.box-body -->
               </div>
               {!! $posts->render() !!}
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@stop