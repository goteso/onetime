@extends('layout.admin')
@section('content')
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         <strong>Treasure Box List</strong>
      </h1>
 
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-info">
         <div class="box-header"  >
            <nav class="navbar navbar-default"  >
               <!---<div class="container-fluid">
                  <div class="navbar-header">
                    <b class="navbar-brand" >Filter By Status :</b>
                  </div>
                  <ul class="nav navbar-nav">
                  <li><a href="{{ URL::to('admins/admin/status/1') }}">Active({{ \App\Admin::where(['status' => 1])->get()->count() }})</a></li>
                    <li><a href="{{ URL::to('admins/admin/status/0') }}">Blocked({{ \App\Admin::where(['status' => 0])->get()->count() }})</a></li>
                  </ul>
                  
                  </div>-->
            </nav>
			
				<?php if(auth()->user()->user_type == 'brand'   ) { ?>
       <a href="{{ URL::to('treasure_box/create') }}" class="pull-right btn btn-info btn-sm"   ><i class="fa fa-plus"></i> Add New</a>
	   <?php } ?>
         </div>
         <!-- /.box-header -->
      </div>
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">Treasure Box List</h3>
               </div>
               <!-- /.box-header -->
               @if(Session::has('message'))
               <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif
               <div id="loading"></div>
               <style>
                  #loading {
                  position: fixed;
                  top: 50%;
                  left: 50%;
                  -webkit-transform: translate(-50%, -50%);
                  transform: translate(-50%, -50%);
                  }
               </style>
               <div id="item-lists">
                  <div class="box-body">
                     <table id="example1" class="table table-bordered table-striped">
                        <thead>
                           <tr>
                              <th>Sr. No.</th>
                              <th>Description</th>
                              <th>Points</th>
               
                              <th>Approved ?</th>
							    <th>Users Points History</th>
           
                              <th>Created</th>
								<?php if(auth()->user()->user_type == 'brand')
			{?>  <th>Action</th><?php } ?>
                    
                           </tr>
                        </thead>
                        <tbody>
                           <?php $sr = 0; foreach ($posts as $key => $value) { ?>
                           <tr>
                              <td><?php echo ++$sr; ?></td>
                              <td><?php 
						 
								   echo  $value['description'];
							  
							  ?></td>
							  
							     <td><?php 
						 
								   echo  $value['points'];
							  
							  ?></td>
                     
                              <td><?php
                                 if($value['approved_status'] == 0 )
                                 	{
                                 	echo "Not Approved";
									?></br>
									<?php if( auth()->user()->user_type == 'admin' ) { ?> <a class="btn btn-small btn-success" title="Delete Treasure Box" href="{{ URL::to('/treasure_box_approve/'.$value['id'].'/treasure_box_approve') }}" onclick="return confirm('Are you sure?')">Approve</a> <?php } ?>
                                  <?php }
                                   else
                                   {
                                    echo "Approved";
                                 } ?>
                              </td>
							  <td>            <a class="btn btn-small btn-info" title="Edit User" href="{{ URL::to('/treasure_box_activities/'.$value['id'].'/treasure_box_activities') }}">Users History</a></td>
							     <td><?php echo $value['created_at']; ?></td>
							
                           
						   	
			<?php if(auth()->user()->user_type == 'brand')
			{?>
							<td>
							                   <a class="btn btn-small btn-info" title="Edit User" href="{{ URL::to('/treasure_box/'.$value['id'].'/edit_treasure_box') }}"><i class="fa fa-pencil"></i></a>                  
                                               <a class="btn btn-small btn-danger" title="Delete User" href="{{ URL::to('/treasure_box/'.$value['id'].'/delete_treasure_box') }}" onclick="return confirm('Are you sure?')"><i class="fa fa-remove"></i></a>   
				              </td>
							  
			<?php } ?>
                 
                           
                     
                           </tr>
                           <?php }?>
                        </tbody>
                     </table>
                  </div>
                  <!-- /.box-body -->
               </div>
               {!! $posts->render() !!}
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@stop