                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
                        <th>Email</th>
                        <th>Address</th>
						<th>QrCode Image</th>
                        <th>Beacon UDID</th>
                        <th>Status</th>
                        <th>Created</th>
                        <th id="action">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($users as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
                   	<td><?php echo $value['email']; ?></td>
                    <td><?php echo $value['long_address']; ?></td>
					<td><?php echo '<img src="'.url('/').'/'.$value['store_qrcode'].'" height="200px" alt="">';?></td>
                    <td><?php echo $value['store_beacon_UDID']; ?></td>
                    
                   	<td><?php
                   	 if($value->status == 0)
                   	 	{
                   	 	echo "Inactive";
                   	   }
                   	   else
                   	   {
                   	    echo "Active";
                   		} ?>
                   	</td>
                    <td><?php echo $value['created_at']; ?></td>
                    <td>  
                                
                   <a class="btn btn-small btn-info" title="Edit " href="{{ URL::to('brands/brand/store/'.$value['id'].'/edit') }}"><i class="fa fa-pencil"></i></a>                  
                  <a class="btn btn-small btn-danger" title="Delete"  href="{{ URL::to('brands/brand/'.$value['id'].'/delete') }}" onclick="return confirm('Are you sure?')"><i class="fa fa-remove"></i></a>   

				  <?php if($value['status'] == '1') { ?>
				  
                   <a class="btn btn-small btn-danger" title="Block" href="{{ URL::to('brands/brand/'.$value['id'].'/block') }}" onclick="return confirm('Are you sure to Block this User?')"><i class="fa fa-ban"></i></a>  
				  <?php } else { ?>
                   <a class="btn btn-small btn-danger" title="Unblock " href="{{ URL::to('brands/brand/'.$value['id'].'/unblock') }}"" onclick="return confirm('Are you sure to Unblock this User?')"><i class="fa fa-unlock"></i></a>  
				  <?php } ?>
 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                