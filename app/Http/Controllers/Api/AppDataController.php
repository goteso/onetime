<?php

namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use App\AppData;
use Session;
use DB;
use Validator;
use App\TermsConditions;
use Carbon\Carbon;
use Hash;
use Mail;
use File;
use App\Traits\one_signal; // <-- you'll need this line...


class AppDataController extends Controller 
{
   public function get_app_data(Request $request)
    {
 
	 
	                $TermsConditions = @\App\AppData::get();
	                $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Successfully';
                    $data['data'] = $TermsConditions; 
                    return $data;
    }
	
	
	
	
	
	
	
	   public function get_init_data(Request $request)
    {
		
		 
		            $init_data = array();
					$init_data['membership_card_types'] = \App\MemberShipCardTypes::get(['id','title','image']);
	 
	             
	                $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Data Fetched Successfully';
                    $data['data'] = $init_data ;
                    return $data;
    }
	
	
	
	
			     //For update user profile
    public function upload_user_card(Request $request)
    {   
	    $validator = Validator::make($request->all(), [
                'user_id' => 'required',
		 
               ]);
           if ($validator->errors()->all()) 
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
            }
            else
            {   

                $user_data = \App\User::where('id',$request->user_id)->get();
                    if(count($user_data) != 0)
                    {  
                        //for file upload
                        $path = public_path().'/users/'.$request->user_id;
                        if(!File::exists($path)) 
                        {
                        File::makeDirectory($path, $mode = 0777, true, true);
                        }
						
						
                        if(isset($request->card_image) && !empty($request->card_image))
                        {
                        $unique_string = 'card-image-'.strtotime(date('Y-m-d h:i:s'));    
                        $file = $request->card_image;                   
                        $photo_name = $unique_string.$file->getClientOriginalName();
                       // $thumb_name = "thumb-". $photo_name;                     
                        $file->move($path,$photo_name);
                       // $this->make_thumb($path.'/'.$photo_name,$path.'/'.$thumb_name,'800');
                       }
					   else
					   {
						$photo_name = '';   
					   }
					  
  					   $data['status_code']  =   1;
                       $data['status_text']    =   'Success';           
                       $data['message']        =   'Card Image is Uploaded.';
                       $data['data'][]["image_url"] =  $request->user_id.'/'.$photo_name;
					   
                    }
                    else
                    {
                       $data['status_code']  =   0;
                       $data['status_text']    =   'Failed';           
                       $data['message']        =   'User not found';
                    }   
                
           }
        return $data;
    }
	
	
	
	
	
}