<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberShipCardTypes extends Model
{
        protected $fillable = [ 'title','image','linked_brand_id'];
		protected $table = 'membership_card_types';
}