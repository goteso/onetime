<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersReviews extends Model
{
        protected $fillable = [ 'recipient_id', 'user_id','review'];
		protected $table = 'users_reviews';
		
			    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
}