<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OneTimePayments extends Model
{
        protected $fillable = [ 'user_id', 'receiver_id','amount','notes'];
		protected $table = 'one_time_payments';
}