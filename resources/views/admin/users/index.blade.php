@extends('layout.admin')
@section('content')

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Users</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/user') }}">Users</a></li>
            <li class="active">Listing</li>
          </ol>
        </section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <!-- Main content -->
        <section class="content">
            <div class="box ">
                
				 <nav class="navbar filters-nav">
 
    <div class="navbar-header">
      <div class="navbar-brand" >Filter By Status :</div>
    </div>
    <ul class="nav navbar-nav">
	 <li><a href="{{ URL::to('admin/user/status/1') }}">Active({{ \App\User::where(['status' => 1])->get()->count() }})</a></li>
      <li><a href="{{ URL::to('admin/user/status/0') }}">Blocked({{ \App\User::where(['status' => 0])->get()->count() }})</a></li>
	  <li><a href="{{ URL::to('admin/user/verified/1') }}">Verified({{ \App\User::where(['verified' => 1])->get()->count() }})</a></li>
	  <li><a href="{{ URL::to('admin/user/verified/0') }}">UnVerified({{ \App\User::where(['verified' => 0])->get()->count() }})</a></li>
 
    </ul>
   
  
</nav>


<input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >


                  <a href="{{ URL::to('admin/user/create') }}" class="pull-right btn btn-info btn-sm" style="display:none" ><i class="fa fa-plus"></i> Add New</a>
                </div><!-- /.box-header -->
             
			
			
			
	
	
	
	
          <div class="row">
            <div class="col-xs-12">
		 
 

              <div class="box">
 
	   
			  
               <!-- <div class="box-header">
                  <h3 class="box-title">User Listing</h3>
                </div><!-- /.box-header -->
                @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif

			   <div id="loading"></div>

<style>
#loading {
    position: fixed;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}

</style>
			   			<div id="item-lists">
						
		@include('admin.users.data-ajax')
	</div>
			   {!! $users->render() !!}
			   
			   
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	  
	  
	  
 
		{{ csrf_field() }}
 
	


	  
 
 
 

     
@stop