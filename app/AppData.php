<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppData extends Model
{
        protected $fillable = [ 'key_name','value'];
		protected $table = 'app_data';
}